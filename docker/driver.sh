#!/bin/bash

CUDA_FILE=cuda_7.5.18_linux.run
DRIVER_FILE=NVIDIA-Linux-x86_64-352.63.run 

wget http://developer.download.nvidia.com/compute/cuda/7.5/Prod/local_installers/${CUDA_FILE}
wget http://it.download.nvidia.com/XFree86/Linux-x86_64/352.63/${DRIVER_FILE}

sudo service lightdm stop
chmod +x ./${DRIVER_FILE}
sudo ./${DRIVER_FILE} -s -N --no-cc-version-check -X 

chmod +x ./${CUDA_FILE} 
./${CUDA_FILE} --toolkit --silent --samples --override --samplespath=`pwd`/..

#fix for compiler check of cuda
sed -e '/if __GNUC__ > 4 || (__GNUC__ == 4 && __GNUC_MINOR__ > 9)/,+5 d' /usr/local/cuda/include/host_config.h > new_file
sudo mv new_file /usr/local/cuda/include/host_config.h
rm new_file

sudo service docker start
sudo gpasswd -a ${USER} docker
sudo service docker restart

docker build -t pelars_client .
cd `pwd`/../NVIDIA_CUDA-7.5_Samples/0_Simple/matrixMul
make 
sudo gpasswd -a ${USER} docker
reboot

